#include <stdio.h>
#include <stdlib.h>

#define MEM_MAX 30000
#define TAB_SIZE 4

unsigned char m[MEM_MAX];
unsigned char *c=NULL;
size_t i,n=0,cp=0,mp=0,scp=0;
ssize_t d=0;
FILE *f;
size_t ln,cl;

void getpos(size_t p) {
	size_t i;
	ln=1;cl=1;
	for(i=0;i<p;i++) {
		if(c[i]=='\n') {ln++;cl=0;}
		if(c[i]=='\t') cl+=TAB_SIZE; else cl++;
	}
}

int main(int argc,char **argv) {

	if(argc<2) {
		printf("usage: %s <script name>\n",argv[0]);
		return -1;
	}
        
	if((f=fopen(argv[1],"rb"))==NULL) {
		printf("error opening file %s\n",argv[1]);
		return -1;
	} 
        
	fseek(f,0L,SEEK_END);
	n=ftell(f);
	rewind(f);
	
	if((c=malloc(sizeof(*c)*n))==NULL) {
		printf("error allocating memory\n");
		return -1;
	}

	if(fread(c,sizeof(*c),n,f)!=n) {
		printf("error reading file %s\n",argv[1]);
		return -1;
	}

	fclose(f);

	for(i=0;i<MEM_MAX;i++) m[i]=0;
	
	while(cp<n) {
		switch(c[cp]) {
			case '.': putchar(m[mp]); break;
			case ',': {int ch=getchar(); m[mp]=(ch==EOF?0:ch);} break;
			case '-': m[mp]--; break;
			case '+': m[mp]++; break;
			case '<': if(mp>0) mp--;         else { getpos(cp); printf("error %i:%i memory out of bounds '<'\n",ln,cl); return -1; } break;
			case '>': if(mp<MEM_MAX-1) mp++; else { getpos(cp); printf("error %i:%i memory out of bounds '>'\n",ln,cl); return -1; } break;
			case '[':
				if(m[mp]==0) {
					scp=cp;
					d=1;
					while(d) {
						if(cp<n) cp++; else { getpos(scp); printf("error %i:%i unmatch '['\n",ln,cl); return -1; } 
						d+=(c[cp]=='[')-(c[cp]==']');
					}
				}
			break;
			case ']': 
				scp=cp;
				d=1;
				while(d) {
					if(cp>0) cp--; else { getpos(scp); printf("error %i:%i unmatch ']'\n",ln,cl); return -1; } 
					d-=(c[cp]=='[')-(c[cp]==']');
				}
				cp--;
			break;
		}
		cp++;
	}

	return 0;
}
